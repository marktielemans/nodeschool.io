/// <reference path="typings/node/node.d.ts"/>
var url = require('url');
var http = require('http');
var port = Number(process.argv[2]);

http.createServer(function (req, res) {
	var u = url.parse(req.url, true);
	
	if (u.path.indexOf('unixtime') > -1) {
		parseUnixTime(u.query, res);
	} else {
		parseTime(u.query, res);
	}
}).listen(port);

var parseUnixTime = function (query, res) {
	var time = new Date(query.iso);
	var customTime = {
		unixtime : time.getTime()
	};
	
	res.writeHead(200, {'Content-Type' : 'application/json' });
	res.end(JSON.stringify(customTime));
};

var parseTime = function (query, res) {
	var time = new Date(query.iso);
	var customTime = {
		hour : time.getHours(),
		minute : time.getMinutes(),
		second : time.getSeconds()	
	};
	
	res.writeHead(200, {'Content-Type' : 'application/json' });
	res.end(JSON.stringify(customTime));
};