/// <reference path="typings/node/node.d.ts"/>
var http = require('http');
var completed = 0;
var webcalls = [
	{ url : process.argv[2], content : '' },
	{ url : process.argv[3], content : '' },
	{ url : process.argv[4], content : '' }
];

webcalls.forEach(function (call) {
	http.get(call.url, function (response) {
		response.setEncoding('utf-8');
		response.on('data', function (data) {
			call.content += data;
		});
		response.on('end', function() {
			completed++;
			
			if (completed == webcalls.length) {
				webcalls.forEach(function (call) {
					console.log(call.content);
				});
			}
		});
	});
});