/// <reference path="typings/node/node.d.ts"/>
var http = require('http');
var throughmap = require('through2-map');
var port = process.argv[2];

http.createServer(function(req, res) {
	req.setEncoding('utf-8');
	req.pipe(throughmap(function (chunk) {
		return chunk.toString().toUpperCase();
	})).pipe(res);
}).listen(port);