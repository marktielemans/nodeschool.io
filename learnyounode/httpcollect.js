/// <reference path="typings/node/node.d.ts"/>
var http = require('http');

http.get(process.argv[2], function(response) {
	var data = "";
	
	response.setEncoding('utf-8');
	response.on('data', function (chunk) {
		data += chunk;
	});
	
	response.on('end', function () {
		console.log(data.length);
		console.log(data);
	});
});