var fs = require('fs');
var pathUtils = require('path');

module.exports = function (path, ext, callback) {
  var filtered = [];

  fs.readdir(path, function (err, data) {
    if (err) {
      return callback(err);
    } else if (!data) {
      return callback('No data!');
    }

    data.forEach(function (e) {
      if (pathUtils.extname(e) === '.' + ext) {
        filtered.push(e);
      }
    });

    return callback(null, filtered);
  });
};
