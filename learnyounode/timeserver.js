/// <reference path="typings/node/node.d.ts"/>
var net = require('net');
var port = process.argv[2];

net.createServer(function callback(socket) {
	socket.end(getFormattedTime(new Date()) + "\n");
}).listen(port);

/** YYYY-MM-DD hh:mm */
var getFormattedTime = function (date) {
	var str = "";
	str += date.getFullYear();
	
	if (date.getMonth() < 9) {
		str += "-0" + (date.getMonth() + 1);
	} else {
		str += "-" + (date.getMonth() + 1);
	}
	
	str += "-" + date.getDate();
	str += " " + date.getHours();
	str += ":" + date.getMinutes();
	return str;
};