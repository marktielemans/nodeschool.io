var fs = require('fs');

var callback = function (err, data) {
  if (err) {
    console.log('Error: ' + err);
    return;
  }

  var newlines = data.split("\n").length - 1;
  console.log(newlines);
};

fs.readFile(process.argv[2], 'utf-8', callback);
