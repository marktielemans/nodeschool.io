/// <reference path="typings/node/node.d.ts"/>
var http = require('http');

http.get(process.argv[2], function (response) {
	response.setEncoding('utf-8');
	
	response.on('data', function (data) {
		console.log(data);
	});
	
	response.on('error', function (err) {
		console.log('Error: ' + err);
	});
});