# Mark's nodeschool repo #

My solutions to some of the challenges posed at [nodeschool](http://nodeschool.io).

### Get started ###
Enter one of the project top-level subfolders and execute 
```
#!terminal

$ npm install
```

Install the appropriate module, for example:

```
#!terminal

$ npm install -g expressworks
```

Run or verify a challenge, for example:

```
#!terminal

$ expressworks verify param.js
```