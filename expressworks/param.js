/// <reference path="typings/node/node.d.ts"/>
var express = require('express');
var crypto = require('crypto');
var app = express();
var port = process.argv[2];

app.put('/message/:id', function (req, res) {
	var hash = crypto.createHash('sha1')
	.update(new Date().toDateString() + req.params.id)
	.digest('hex');
	res.end(hash);
});
app.listen(port);