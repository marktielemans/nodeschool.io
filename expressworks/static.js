/// <reference path="typings/node/node.d.ts"/>
var express = require('express');
var app = express();
var port = process.argv[2];
var publicFolder = process.argv[3];

app.use(express.static(publicFolder));
app.listen(port);