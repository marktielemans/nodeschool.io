/// <reference path="typings/node/node.d.ts"/>
var express = require('express');
var stylus = require('stylus');
var app = express();
var port = process.argv[2];
var publicFolder = process.argv[3];

app.use(express.static(publicFolder));
app.use(stylus.middleware(publicFolder));

app.listen(port);