/// <reference path="typings/node/node.d.ts"/>
var fs = require('fs');
var express = require('express');
var app = express();
var port = process.argv[2];
var fileName = process.argv[3];

app.get('/books', function (req, res) {
	fs.readFile(fileName, function (err, data) {
		res.send(JSON.parse(data.toString()));
	});
});

app.listen(port);